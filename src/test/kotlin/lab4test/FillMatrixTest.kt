package com.sandbox.laba4

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.assertThrows

class FillMatrixTest {
    val factory = { mtrxSize: Int -> SquareMatrix(mtrxSize) }

    @Test
    fun testMatrixOfSizeException() {
        assertThrows(IllegalArgumentException::class.java) {
            BuilderMatrix.build<SquareMatrix>( 0, factory)
        }
    }

    @Test
    fun `size 1`() {
        val testMatrix: SquareMatrix = BuilderMatrix.build<SquareMatrix>(1, factory)
        val realMatrix = SquareMatrix(1)

        realMatrix.setCell(0, 0, 1)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 1))

        assertEquals(realMatrix, testMatrix)
    }

    @Test
    fun `size 2`() {
        val testMatrix: SquareMatrix = BuilderMatrix.build<SquareMatrix>( 2, factory)
        val realMatrix = SquareMatrix(2)

        realMatrix.setCell(0, 0, 4)
        realMatrix.setCell(1, 0, 1)
        realMatrix.setCell(0, 1, 3)
        realMatrix.setCell(1, 1, 2)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 2))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 2))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 2))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 2))

        assertEquals(realMatrix, testMatrix)
    }

    @Test
    fun `size 3`() {
        val testMatrix: SquareMatrix = BuilderMatrix.build<SquareMatrix>( 3, factory)
        val realMatrix = SquareMatrix(3)

        realMatrix.setCell(0, 0, 7)
        realMatrix.setCell(1, 0, 8)
        realMatrix.setCell(2, 0, 1)
        realMatrix.setCell(0, 1, 6)
        realMatrix.setCell(1, 1, 9)
        realMatrix.setCell(2, 1, 2)
        realMatrix.setCell(0, 2, 5)
        realMatrix.setCell(1, 2, 4)
        realMatrix.setCell(2, 2, 3)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 3))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 3))
        testMatrix.setCell(2, 0, FillMatrix.getCell(2, 0, 3))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 3))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 3))
        testMatrix.setCell(2, 1, FillMatrix.getCell(2, 1, 3))
        testMatrix.setCell(0, 2, FillMatrix.getCell(0, 2, 3))
        testMatrix.setCell(1, 2, FillMatrix.getCell(1, 2, 3))
        testMatrix.setCell(2, 2, FillMatrix.getCell(2, 2, 3))

        assertEquals(realMatrix, testMatrix)
    }

    @Test
    fun `size 4`() {
        val testMatrix: SquareMatrix = BuilderMatrix.build<SquareMatrix>( 4, factory)
        val realMatrix = SquareMatrix(4)

        realMatrix.setCell(0, 0, 10)
        realMatrix.setCell(1, 0, 11)
        realMatrix.setCell(2, 0, 12)
        realMatrix.setCell(3, 0, 1)
        realMatrix.setCell(0, 1, 9)
        realMatrix.setCell(1, 1, 14)
        realMatrix.setCell(2, 1, 13)
        realMatrix.setCell(3, 1, 2)
        realMatrix.setCell(0, 2, 8)
        realMatrix.setCell(1, 2, 15)
        realMatrix.setCell(2, 2, 16)
        realMatrix.setCell(3, 2, 3)
        realMatrix.setCell(0, 3, 7)
        realMatrix.setCell(1, 3, 6)
        realMatrix.setCell(2, 3, 5)
        realMatrix.setCell(3, 3, 4)

        testMatrix.setCell(0, 0, FillMatrix.getCell(0, 0, 4))
        testMatrix.setCell(1, 0, FillMatrix.getCell(1, 0, 4))
        testMatrix.setCell(2, 0, FillMatrix.getCell(2, 0, 4))
        testMatrix.setCell(3, 0, FillMatrix.getCell(3, 0, 4))
        testMatrix.setCell(0, 1, FillMatrix.getCell(0, 1, 4))
        testMatrix.setCell(1, 1, FillMatrix.getCell(1, 1, 4))
        testMatrix.setCell(2, 1, FillMatrix.getCell(2, 1, 4))
        testMatrix.setCell(3, 1, FillMatrix.getCell(3, 1, 4))
        testMatrix.setCell(0, 2, FillMatrix.getCell(0, 2, 4))
        testMatrix.setCell(1, 2, FillMatrix.getCell(1, 2, 4))
        testMatrix.setCell(2, 2, FillMatrix.getCell(2, 2, 4))
        testMatrix.setCell(3, 2, FillMatrix.getCell(3, 2, 4))
        testMatrix.setCell(0, 3, FillMatrix.getCell(0, 3, 4))
        testMatrix.setCell(1, 3, FillMatrix.getCell(1, 3, 4))
        testMatrix.setCell(2, 3, FillMatrix.getCell(2, 3, 4))
        testMatrix.setCell(3, 3, FillMatrix.getCell(3, 3, 4))


        assertEquals(realMatrix, testMatrix)
    }

}
