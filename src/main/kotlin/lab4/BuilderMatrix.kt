package com.sandbox.laba4

/**
 * Сатический класс билдер для матрицы.
 * Имеет метод для создания матрицы.
 * @param mtrxSize - размер матрицы любое значение Int > 0.
 * @return Возвращает пустую матрицу SquareMatrix размера mtrxSize.
 */

object BuilderMatrix {
    fun <T> build(mtrxSize: Int, factory: (Int) -> T): T {
        if (mtrxSize.hashCode() <= 0) {
            throw IllegalArgumentException("Array Invalid")
        }
        //val factory = { mtrxSize: Int -> SquareMatrix(mtrxSize) }
        val a = factory(mtrxSize)
        return a
    }
}
