package com.sandbox.laba4

object FillMatrix {
    /**
     * 16 вариант. Заполнение матрицы по спирали. Матрица заполняется сверху с правого столбца.
     * В рисунке транспонируются чётные П-образные полуспирали.
     * @param x - колонка матрицы с любыми Int >= 0; х < n; отсчет начинается с 0.
     * @param y - строка матрицы с любыми Int >= 0; y < n; отсчет начинается с 0.
     * @param n размер матрицы - любые Int > 0.
     * @return Возвращает значение Int в ячейке по координатам x и y в соответствии с алгоритмом рисунка.
     */

    fun getCell(x: Int, y: Int, n: Int): Int {
        /*
        метод вычисления значения в ячейке
        */

        if (x == n - 1) {
            return y + 1
        } else {
            var a = x + 1
            var b = y + 1

            val spiral = Math.min(Math.min(b, (n - b) + 1), a) //виток спирали
            val innerNx = n - spiral
            val innerNy = n - ((spiral - 1) * 2)
            a -= spiral - 1
            b -= spiral - 1

            if (spiral % 2 == 1) {
                b = (innerNy + 1) - b
            }

            if (b == 1 || b == innerNy) {// вычисление по горизонтали
                if (b == 1) {
                    a = (innerNx + 1) - a
                } else {
                    a = (innerNx + (innerNy - 2)) + a
                }
            } else {
                a = (innerNx) + (b - 1)
            } // вычисление по вертикали

            return a + ((n * n) - (innerNx * innerNy))
        }
    }
}
