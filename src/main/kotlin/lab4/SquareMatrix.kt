package com.sandbox.laba4

/**
 * Класс для создания квадратной матрицы.
 * В конструкторе класса создается массив заданного размера.
 * @param _size размер матрицы. Любые Int > 0.
 */

class SquareMatrix(_size: Int) {
    private var size: Int = _size
    private var array: Array<IntArray> = Array(_size) { IntArray(_size) }
    fun getSize(): Int {
        return size
    }

    fun getCell(x: Int, y: Int): Int {
        return array[x][y]
    }

    fun setCell(x: Int, y: Int, value: Int) {
        array[x][y] = value
    }

    override fun equals(other: Any?): Boolean {
        if (other is SquareMatrix) {
            if (this.getSize() == other.getSize()) {
                var x: Int
                var y = 0

                while (y < this.getSize()) {
                    x = 0
                    while (x < this.getSize()) {
                        if (this.getCell(x, y) != other.getCell(x, y)) {
                            return false
                        }
                        x++
                    }
                    y++
                }
                return true
            } else return false
        } else return false
    }
}
